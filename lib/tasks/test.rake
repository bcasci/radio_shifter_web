namespace :test do
  Rails::TestTask.new("lib" => "test:prepare") do |t|
    t.pattern = "test/lib/**/*_test.rb"
  end
  Rake::Task["test:units"].enhance ["test:lib"]
  Rake::Task["test:run"].enhance ["test:lib"]

  Rails::TestTask.new("extras" => "test:prepare") do |t|
    t.pattern = "test/extras/**/*_test.rb"
  end
  Rake::Task["test:units"].enhance ["test:extras"]
  Rake::Task["test:run"].enhance ["test:extras"]
end
