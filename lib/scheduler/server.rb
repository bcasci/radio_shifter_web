module Scheduler
  class Server
    include ::Scheduler::JobTransformations

    def initialize(scheduler)
      @scheduler = scheduler
      @job_editor = Scheduler::JobEditor.new(@scheduler)
      @lock = Mutex.new
    end

    def cron(recording_schedule)
      @lock.synchronize do
        @job_editor.cron(recording_schedule)
      end
    end

    def at(recording_schedule)
      @lock.synchronize do
        @job_editor.at(recording_schedule)
      end
    end

    def unschedule(job_id)
      @lock.synchronize do
        @job_editor.unschedule(job_id)
      end
    end

    def job(job_id)
      begin
        job = @scheduler.job(job_id)
        job_to_hash(job) if job
      rescue ArgumentError => e
        nil
      end
    end

    def jobs(job_ids)
      found_jobs = job_ids.map do |id|
        job(id)
      end

      found_jobs.compact
    end
  end
end
