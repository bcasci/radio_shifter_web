require 'pty'
require 'uri'

module Scheduler
  class Recorder
    attr_accessor :folder, :pid
    PLAYLIST_FORMATS = %w{.asx .m3u .m3u8 .pls .qtl .ram .sdp}
    def initialize(url, duration, folder)
      @uri = URI.parse(url)
      @dump_files = []
      @duration = duration
      @folder = folder
    end

    def command
      playlist_option = if PLAYLIST_FORMATS.any? { | format | @uri.path.end_with?(format) }
        '-playlist'
      end

      "mplayer -quiet -dumpaudio -dumpfile #{get_dump_file_name} #{playlist_option} #{@uri}"
    end

    def dump_files
      @dump_files.select {|dump_file| File.exists?(dump_file)}
    end

    def start
      @dump_files.clear
      stop_at = Time.now + @duration
      until stop_time_reached?(stop_at)
        output, input, @pid = PTY.spawn(command)
        watch_process(@pid, output, stop_at)
        cleanup_after_process(@pid, output, input)
        sleep 1
      end
    end

    private

    def cleanup_after_process(pid, output, input)
      output.close unless output.closed?
      input.close unless input.closed?
      begin
        Process.wait(pid)
      rescue Errno::ECHILD
      end
    end

    def get_dump_file_name
      part_no = (@dump_files.size + 1).to_s.rjust(4,'0')
      @dump_files << "#{@folder}/part_#{part_no}.dump"
      @dump_files.last
    end

    def watch_process(pid, output, stop_at)
      until stop_time_reached?(stop_at)
        begin
          PTY.check(pid, true)
          output.read(512) unless output.closed?
        rescue PTY::ChildExited
          break
        rescue Errno::EIO
          break
        end
      end
    end

    def stop_time_reached?(stop_at)
      Time.now >= stop_at
    end
  end
end
