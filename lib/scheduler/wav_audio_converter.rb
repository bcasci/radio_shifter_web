module Scheduler
  class WavAudioConverter

    attr_reader :converted_files

    def initialize(ffmpeg_path, files)
      @ffmpeg_path = ffmpeg_path
      @files = files
      @converted_files = []
    end

    def convert
      @converted_files.clear
      @files.map do |file|
        @converted_files << convert_file(file)
        @converted_files.last
      end
    end

    private

    def command(file)
      "#{@ffmpeg_path} -y -i #{file} #{output_file(file)}"
    end

    def convert_file(file)
      command_output, converted = run_command(command(file))
      if converted
        return output_file(file)
      else
        error = CommandLineError.new(command_output)
        raise error, "error with command"
      end
    end

    def output_file(file)
      file.split('.').first + '.wav'
    end

    def run_command(cmd)
      output = `#{cmd} 2>&1`
      [output, $?.success?]
    end
  end
end
