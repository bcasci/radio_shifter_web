module Scheduler
  class JobEditor
    include ::Scheduler::JobTransformations

    def initialize(scheduler)
      @scheduler = scheduler
    end

    def cron(recording_schedule)
      edit(recording_schedule) do
        @scheduler.cron(recording_schedule.rufus_cron_line) do |job|
          Scheduler::JobExecutor.new(recording_schedule, job).execute
        end
      end
    end

    def at(recording_schedule)
      edit(recording_schedule) do
        @scheduler.at(recording_schedule.rufus_at_line) do |job|
          Scheduler::JobExecutor.new(recording_schedule, job).execute
        end
      end
    end

    def unschedule(job_id)
      begin
        @scheduler.unschedule(job_id)
      rescue ArgumentError => e
      end
    end

    private

    def edit(recording_schedule)
      unschedule(recording_schedule.job_id)
      job_id = yield
      job = @scheduler.job(job_id)
      job_to_hash(job)
    end
  end
end
