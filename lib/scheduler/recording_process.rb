module Scheduler
  class RecordingProcess
    attr_reader :recording_schedule

    def initialize(recording_schedule)
      @recording_schedule = recording_schedule
      @pid_file_name = @recording_schedule.pid_file_name
    end

    def spawn
      return if running?
      logfile_path_option = if ENV['RECORDINGS_LOG_FOLDER']
        "-l #{ENV['RECORDINGS_LOG_FOLDER']}/#{recording_schedule.id}/recording.log"
      end

      pid_file_option = "-p #{@pid_file_name}"

      command = "cd #{Rails.root}/bin && FFMPEG_CMD=#{ ENV['FFMPEG_CMD'] } FFPROBE_CMD=#{ ENV['FFPROBE_CMD'] } ./record_stream -i #{ @recording_schedule.id } -u #{ @recording_schedule.stream_url } -d #{ @recording_schedule.duration } -f /tmp/radio_shifter_recording/#{ @recording_schedule.id } #{logfile_path_option} #{pid_file_option} > /dev/null"
      pid = Process.spawn(command)
      Process.detach(pid)
    end

    # TODO - Stop using PID to prevent duplicate recordings.
    # Have the recorder get a fail lock and exit on fail
    # We still want the PID for other reasons, so have the recorder
    # send it's PID to message queue when it starts, and wipe the PID
    # when it stops.
    def running?
      if File.exists?(@pid_file_name)
        pid = File.read(@pid_file_name).strip.to_i
        begin
          Process.kill(0, pid) == 1
        rescue Errno::ESRCH
          false
        end
      else
        false
      end
    end
  end
end
