module Scheduler
  module JobTransformations
    def job_to_hash(job)
      { id: job.id, last_time: job.last_time, next_time: job.next_time }
    end
  end
end
