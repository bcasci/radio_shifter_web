module Scheduler
  class AudioProber
    def initialize(ffprobe_path, file, logger = nil)
      @ffprobe_path = ffprobe_path
      @file = file
      @logger = logger
    end

    def bitrate
      probe['bit_rate'].to_i
    end

    def probe
      command_output, success = run_command(command(@file))
      command_output_to_hash(command_output)
    end

    private

    def command(file)
      "#{@ffprobe_path} -loglevel quiet -show_streams #{@file}"
    end

    def command_output_to_hash(command_output)
      attributes = {}
      command_output.split("\n")[1..-2].each do |entry|
        key, value = entry.split('=')
        attributes[key] = value
      end
      attributes
    end

    def run_command(cmd)
      output = `#{cmd} 2>&1`
      if @logger
        @logger.info cmd
        @logger.info output
      end
      [output, $?.success?]
    end
  end
end
