module Scheduler
  class AudioConcatenator

    def initialize(ffmpeg_command, files, output_file_name)
      @ffmpeg_command = ffmpeg_command
      @files = files
      @output_file_name = output_file_name
    end

    def concatenate
      command_output, concatenated = run_command(command(@files))

      if concatenated
        concatenated
      else
        error = CommandLineError.new(command_output)
        raise error, "error with command"
      end
    end

    private

    def command(files)
      "cat #{@files.join(' ')} | #{@ffmpeg_command} -i - #{@output_file_name}"
    end

    def run_command(cmd)
      output = `#{cmd} 2>&1`
      [output, $?.success?]
    end
  end
end
