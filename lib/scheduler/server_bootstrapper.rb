module Scheduler
  class ServerBootstrapper
    def initialize(server, recording_schedules_scope)
      @server = server
      @recording_schedules_scope = recording_schedules_scope
    end

    def bootstrap
      @recording_schedules_scope.all.decorate.each do |recording_schedule|
        next if recording_schedule.expired?
        job_hash = if recording_schedule.recurring?
          @server.cron(recording_schedule)
        else
          @server.at(recording_schedule)
        end
        recording_schedule.update_attribute(:job_id, job_hash[:id])
      end
      nil
    end
  end
end
