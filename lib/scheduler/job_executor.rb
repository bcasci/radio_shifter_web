module Scheduler
  class JobExecutor
    def initialize(recording_schedule, job)
      @recording_schedule = recording_schedule
      @job = job
    end

    def execute
      process = RecordingProcess.new(@recording_schedule)
      unless process.running?
        perform_execution(process)
      end
    end

    private

    def perform_execution(process)
      process.spawn
      @recording_schedule.tap do |r|
        r.last_run_at = @job.last_time
        r.next_run_at = @job.next_time
        r.save(validate: false)
      end
    end
  end
end
