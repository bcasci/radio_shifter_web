module Scheduler
  class RecordingProcessManager
    def initialize(schedule_id, recorder, ffmpeg_path, ffprobe_path, logger = nil)
      @schedule_id = schedule_id
      @recorder = recorder
      @ffmpeg_path = ffmpeg_path
      @ffprobe_path = ffprobe_path
      @logger = logger
    end

    def start
      begin
        run
      rescue CommandLineError => e
        if @logger
          @logger.error "Command line error."
          @logger.error "#{e.message}"
          @logger.error "#{e.command_output}"
          @logger.error e.backtrace.join("\n")
        end
      rescue Exception => e
        if @logger
          @logger.error "#{e.message}"
          @logger.error e.backtrace.join("\n")
        end
      end
    end

    def run
      success = false
      @recorder.start
      sleep 5
      # TODO - put the following steps into a separate
      # async action
      wav_converter = Scheduler::WavAudioConverter.new(@ffmpeg_path, @recorder.dump_files)
      wav_files = wav_converter.convert
      finalized_wav_file = get_finalized_wav_file(wav_files)
      if finalized_wav_file
        mp3_bitrate = wave_bitrate_to_mp3_bitrate(
          Scheduler::AudioProber.new(@ffprobe_path, finalized_wav_file, @logger).bitrate
        )
        mp3_file = finalized_wav_file.gsub('.wav', '.mp3')
        mp3_converter = Scheduler::Mp3AudioConverter.new(@ffmpeg_path, finalized_wav_file, mp3_file, mp3_bitrate, @logger)
        if mp3_converter.convert
          Services::RecordingUploader.async.upload(@schedule_id, mp3_file)
          # Stalker.enqueue('finished_recordings',
          #   { schedule_id: @schedule_id, filename: mp3_file },
          #   { ttr: (3600 * 24) }
          # )
          cleanup_recording_folder(@recorder.folder)
          success = true
        end
      end
      success
    end

    private

    def cleanup_recording_folder(folder)
      files = Dir[folder + '/*.*'].select{|f| !File.directory?(f) &&  File.extname(f) != '.mp3' }
      File.delete(*files)
    end

    def get_finalized_wav_file(wav_files)
      if wav_files.size > 1
        output_file = "#{@recorder.folder}/combined.wav"
        concatenator = Scheduler::AudioConcatenator.new(@ffmpeg_path, wav_files, output_file)
        return output_file if concatenator.concatenate
      else
        wav_files.first
      end
    end

    def wave_bitrate_to_mp3_bitrate(wav_bitrate)
      if wav_bitrate <= 500000
        '48k'
      elsif wav_bitrate <= 800000
        '64k'
      elsif wav_bitrate <= 1199999
        '96k'
      else
        '128k'
      end
    end

  end
end
