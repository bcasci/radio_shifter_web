module Scheduler
  class Mp3AudioConverter
    def initialize(ffmpeg_path, input_file, output_file, output_bitrate, logger = nil)
      @ffmpeg_path = ffmpeg_path
      @input_file = input_file
      @output_file = output_file
      @output_bitrate = output_bitrate
      @logger = logger
    end

    def convert
      convert_file
    end

    private

    def command
      "#{@ffmpeg_path} -y -i #{@input_file} -acodec libmp3lame -b:a #{@output_bitrate} -minrate #{@output_bitrate} -maxrate #{@output_bitrate} #{@output_file}"
    end

    def convert_file
      command_output, converted = run_command(command)
      if converted
        return true
      else
        error = CommandLineError.new(command_output)
        raise error, "error with command"
      end
    end

    def run_command(cmd)
      output = `#{cmd} 2>&1`
      if @logger
        @logger.info cmd
        @logger.info output
      end
      [output, $?.success?]
    end
  end
end
