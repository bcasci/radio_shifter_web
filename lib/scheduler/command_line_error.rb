module Scheduler
  class CommandLineError < StandardError
    attr_reader :command_output
    def initialize(command_output)
      @command_output = command_output
    end
  end
end
