#!/bin/bash
: ${FFMPEG_CMD:="ffmpeg"}
: ${FFPROBE_CMD:="ffprobe"}

STREAM_URL=$1
DURATION=$2
SCHEDULE_ID=$3
BASE_FILE_NAME="$(echo $(echo $RANDOM) $(date +%s) | base64 | head -c 20 ; echo)"
PID_FILE="../tmp/pids/mplayer.$BASE_FILE_NAME.pid"
DUMP_FILE="../tmp/recordings/$BASE_FILE_NAME.dump"
WAV_FILE="../tmp/recordings/$BASE_FILE_NAME.wav"
MP3_FILE="../tmp/recordings/$BASE_FILE_NAME.mp3"

send_finished_recording_message ()
{
  local filename=$(basename $1)
  local schedule_id=$2
  local tube_name="finished_recordings"
  local message="[\"$tube_name\",{\"filename\":\"$filename\",\"schedule_id\":\"$schedule_id\"}]"
  local priority=0
  local delay=0
  local time_to_run=7200
  local byte_size=${#message}

  (
    echo open 127.0.0.1 11300;
    sleep .5
    echo use $tube_name
    sleep .5
    echo put $priority $delay $time_to_run $byte_size
    sleep .5
    echo $message
    sleep .5
    echo quit
  ) | telnet
}

nohup mplayer -quiet -dumpaudio -dumpfile $DUMP_FILE $STREAM_URL > /dev/null &
echo $! > $PID_FILE

sleep $DURATION && kill `cat $PID_FILE`
sleep 5

nohup $FFMPEG_CMD -y -i $DUMP_FILE $WAV_FILE

OUTPUT_BITRATE='48k'
WAV_BITRATE=`$FFPROBE_CMD -loglevel quiet -show_streams $WAV_FILE | grep '^bit_rate' | cut -d "=" -f 2`

if [ $WAV_BITRATE -lt 500000 ]; then
  OUTPUT_BITRATE='48k'
elif [ $WAV_BITRATE -lt 800000 ]; then
  OUTPUT_BITRATE='64k'
elif [ $WAV_BITRATE -lt 1199999 ]; then
  OUTPUT_BITRATE='96k'
elif [ $WAV_BITRATE -gt 1200000 ]; then
  OUTPUT_BITRATE='128k'
fi

nohup $FFMPEG_CMD -y -i $WAV_FILE -acodec libmp3lame -b:a $OUTPUT_BITRATE -minrate $OUTPUT_BITRATE -maxrate $OUTPUT_BITRATE $MP3_FILE
rm $PID_FILE
rm $DUMP_FILE
rm $WAV_FILE

send_finished_recording_message $MP3_FILE $SCHEDULE_ID
