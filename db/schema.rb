# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151207044530) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "recording_schedules", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "stream_url"
    t.date     "start_on"
    t.date     "starts_on"
    t.integer  "duration"
    t.string   "days_of_week"
    t.datetime "inactive_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "start_time",   limit: 10
    t.string   "stop_time",    limit: 10
    t.boolean  "recurring",               default: true
    t.string   "job_id",       limit: 50
    t.integer  "pid"
    t.datetime "last_run_at"
    t.datetime "next_run_at"
  end

  add_index "recording_schedules", ["last_run_at"], name: "index_recording_schedules_on_last_run_at", using: :btree
  add_index "recording_schedules", ["next_run_at"], name: "index_recording_schedules_on_next_run_at", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.integer  "uid"
    t.string   "oauth_token"
    t.string   "time_zone"
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["oauth_token"], name: "index_users_on_oauth_token", using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["time_zone"], name: "index_users_on_time_zone", using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree

end
