class AddStopTimeToRecordingSchedules < ActiveRecord::Migration
  def change
    add_column :recording_schedules, :stop_time, :string, limit: 10
  end
end
