class AddPidToRecordingSchedules < ActiveRecord::Migration
  def change
    add_column :recording_schedules, :pid, :integer
  end
end
