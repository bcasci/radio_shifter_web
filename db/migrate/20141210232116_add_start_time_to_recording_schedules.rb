class AddStartTimeToRecordingSchedules < ActiveRecord::Migration
  def change
    add_column :recording_schedules, :start_time, :string, limit: 10
  end
end
