class AddJobIdToRecordingSchedules < ActiveRecord::Migration
  def change
    add_column :recording_schedules, :job_id, :string, limit: 50
  end
end
