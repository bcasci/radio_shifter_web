class AddRecurringToRecordingSchedules < ActiveRecord::Migration
  def change
    add_column :recording_schedules, :recurring, :boolean, default: true
  end
end
