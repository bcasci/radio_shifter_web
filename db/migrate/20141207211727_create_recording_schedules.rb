class CreateRecordingSchedules < ActiveRecord::Migration
  def change
    create_table :recording_schedules do |t|
      t.integer :user_id
      t.string :name
      t.string :stream_url
      t.datetime :starts_time
      t.date :starts_on
      t.integer :duration
      t.string :days_of_week
      t.datetime :inactive_at

      t.timestamps
    end
  end
end
