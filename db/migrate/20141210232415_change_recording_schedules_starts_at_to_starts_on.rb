class ChangeRecordingSchedulesStartsAtToStartsOn < ActiveRecord::Migration
  def up
    rename_column :recording_schedules, :starts_time, :start_on
    change_column :recording_schedules, :start_on, :date
  end

  def down
    rename_column :recording_schedules, :start_on, :starts_time
    change_column :recording_schedules, :start_on, :datetime
  end
end
