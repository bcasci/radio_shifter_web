class AddLastRunAtToRecordingSchedules < ActiveRecord::Migration
  def change
    add_column :recording_schedules, :last_run_at, :datetime
    add_index :recording_schedules, :last_run_at
  end
end
