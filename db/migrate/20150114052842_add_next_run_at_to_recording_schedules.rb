class AddNextRunAtToRecordingSchedules < ActiveRecord::Migration
  def change
    add_column :recording_schedules, :next_run_at, :datetime
    add_index :recording_schedules, :next_run_at
  end
end
