class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :integer
    add_column :users, :oauth_token, :string

    add_index :users, :provider
    add_index :users, :uid
    add_index :users, :oauth_token
  end
end
