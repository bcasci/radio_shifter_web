require File.expand_path("../environment", __FILE__)
require 'stalker'
include Stalker

job "finished_recordings" do |args|
  # upload file to dropbox
  recording_schedule = RecordingSchedule.find(args['schedule_id'])
  file = File.new("#{args['filename']}", 'r')
  Services::RecordingUploader.new(
    recording_schedule,
    file,
    Devise.omniauth_configs[:dropbox_oauth2].strategy.consumer_key,
    Devise.omniauth_configs[:dropbox_oauth2].strategy.consumer_secret
  ).upload
end
