ENV['BEANSTALK_PORT'] ||= '11300'

Backburner.configure do |config|
  config.beanstalk_url       = ["beanstalk://127.0.0.1:#{ENV['BEANSTALK_PORT']}"]
  config.tube_namespace      = "radioshifer.#{Rails.env}"
  config.namespace_separator = "."
  # TODO unlink user accounts that raise DropboxAuthError
  config.on_error            = lambda { |e, name, args, job| AsyncHandlers::ErrorHandler.process(e, name, args, job) }
  config.max_job_retries     = 4
  # default 5 minutes
  config.retry_delay         = 300
  # should retry at 300, 600, 1200, 2700....
  config.retry_delay_proc    = lambda { |min_retry_delay, num_retries| (num_retries * min_retry_delay) * num_retries }
  config.default_priority    = 65536
  config.respond_timeout     = 60 * 60
  config.default_worker      = Backburner::Workers::Forking
  config.logger              = Logger.new(STDOUT)
  config.primary_queue       = "radioshifer-jobs"
  config.reserve_timeout     = nil
end