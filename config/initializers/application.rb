module RadioShifterWeb
  Time::DATE_FORMATS[:time_12_hour] = "%I:%M%p"
  Date::DAYNAMES_MAP = Hash[
    *Date::DAYNAMES.each_with_index.map { |day, i| [day,i] }.flatten
  ]

  ActionView::Base.default_form_builder = FormBuilders::ExtendedFormBuilder
  ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
    if html_tag =~ /type=\"checkbox\"|<label/
      html_tag
    else
      error_spans = instance.error_message.map do |error|
        "<span class=\"error\">#{error}</span>"
      end

      %(<div class='field_with_errors'>#{html_tag}#{error_spans.join}</div>).html_safe
    end
  end
end
