if Rails.env.development?
  ENV['FFMPEG_CMD'] ||= 'ffmpeg'
  ENV['FFPROBE_CMD'] ||= 'ffprobe'
end
