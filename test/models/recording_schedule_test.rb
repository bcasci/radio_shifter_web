require 'test_helper'

class RecordingScheduleTest < ActiveSupport::TestCase
  describe 'Validations' do
    subject { RecordingSchedule.new }

    it 'requires a name' do
      subject.valid?

      subject.errors[:name].to_sentence.must_equal('can\'t be blank')
    end

    it 'requires a start time' do
      subject.valid?

      subject.errors[:start_time].to_sentence.must_equal('can\'t be blank')
    end

    it 'requires a stop time' do
      subject.valid?

      subject.errors[:stop_time].to_sentence.must_equal('can\'t be blank')
    end

    it 'requires a stream url' do
      subject.valid?

      subject.errors[:stream_url].to_sentence.must_equal('can\'t be blank')
    end

    it 'requires a valid stream url' do
      subject.stream_url = "bad url"
      subject.valid?

      subject.errors[:stream_url].to_sentence.must_equal('is an invalid URL')
    end

    it 'requires a valid stream url' do
      subject.stream_url = "bad url"
      subject.valid?

      subject.errors[:stream_url].to_sentence.must_equal('is an invalid URL')
    end

    it 'allows a valid stream url' do
      subject.stream_url = "http://streamurl.com/path"
      subject.valid?

      subject.errors[:stream_url].to_sentence.must_be_empty
    end

    describe 'when recurring' do
      describe 'is true' do
        before do
          subject.recurring = true
        end
        it 'requires days of week' do
          subject.valid?

          subject.errors[:days_of_week].to_sentence.must_equal('must be one or more days of the week')
        end

        it 'requires valid numerical days of week' do
          subject.days_of_week = [-1,7]
          subject.valid?

          subject.errors[:days_of_week].to_sentence.must_equal('must be one or more days of the week')
        end
      end

      describe 'is false' do
        it 'requires a start on date' do
          subject.recurring = false
          subject.valid?

          subject.errors[:start_on].to_sentence.must_equal('can\'t be blank')
        end
      end
    end
  end
end
