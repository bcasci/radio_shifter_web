require 'test_helper'

class RecordingScheduleDecoratorTest < Draper::TestCase
  describe 'Instance methods' do
    describe '#human_readable_schedule' do
      describe 'when schedule is recurring' do
        subject { recording_schedules(:recurring).decorate }

        it 'returns a sentence with start time, end time on weekdays' do
          subject.human_readable_schedule.must_equal 'Between 10:00am and 12:00pm on Monday, Tuesday, Wednesday, Thursday, and Friday'
        end
      end

      describe 'when schedule is not recurring' do
        subject { recording_schedules(:one_time).decorate }

        it 'returns a sentence with start and end times on a date' do
          subject.human_readable_schedule.must_equal 'Between 10:00am and 12:00pm on December 31, 2014'
        end
      end
    end

    describe '#logfile_name' do
      subject { recording_schedules(:one_time).decorate }
      let(:expected_filename) { Rails.root.join("log/recordings/#{subject.id}/recording.log") }

      it 'returns a log file relative to the Rails log folder' do
        subject.logfile_name.must_equal expected_filename
      end
    end

    describe '#pid_file_name' do
      subject { recording_schedules(:one_time).decorate }
      let(:expected_filename) { Rails.root.join("tmp/pids/recordings/#{subject.id}.pid") }

      it 'returns a pid file relative to the Rails log folder' do
        subject.pid_file_name.must_equal expected_filename
      end
    end

    describe '#rufus_cron_line' do
      subject { recording_schedules(:recurring).decorate }

      it 'returns a string formatted for Rufus cron jobs' do
        subject.rufus_cron_line.must_equal '0 10 * * 1,2,3,4,5 America/New_York'
      end
    end

    describe '#rufus_at_line' do
      subject { recording_schedules(:one_time).decorate }

      it 'returns a string formatted for Rufus cron jobs' do
        subject.rufus_at_line.must_equal '2014-12-31 10:00am America/New_York'
      end
    end
  end
end
