require "test_helper"
require 'capybara/poltergeist'
require "capybara/rails"

Capybara.configure do |config|
  config.javascript_driver = :poltergeist
  config.default_driver = :poltergeist
end

OmniAuth.config.test_mode = true
OmniAuth.config.mock_auth[:dropbox_oauth2] = OmniAuth::AuthHash.new(
  provider: 'dropbox',
  uid: '123545',
  info: { email: 'user@gmail.com' },
  credentials: { token: 'xyz' }

)

class ActionDispatch::IntegrationTest
  include Capybara::DSL
  include Warden::Test::Helpers
  Warden.test_mode!

  after do
    Warden.test_reset!
    logout(:user)
  end

  def after_teardown
    super

    if !passed?
      base_file_name = name.gsub(' ','_')
      png_file = Rails.root + "tmp/capybara/#{base_file_name}.png"
      html_file = Rails.root + "tmp/capybara/#{base_file_name}.html"
      page.driver.save_screenshot(png_file, full: true)
      File.open("#{html_file}", "w+" ) { |f| f.write page.html }
      puts
      puts "Screenhot: #{png_file}"
      puts "HTML: #{html_file}"
      puts
    end
  end

  def sign_in(user)
    login_as(user, scope: :user)
  end

  def sign_out
    logout(:user)
  end

end
