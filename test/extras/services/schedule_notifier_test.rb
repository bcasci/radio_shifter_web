require 'test_helper'

class Services::ScheduleNotifierTest < ActiveSupport::TestCase
  describe '#save' do
    describe 'with a existing recurring RecordingSchedule' do
      let(:recording_schedule) { recording_schedules(:recurring).decorate }
      subject { Services::ScheduleNotifier.new(recording_schedule, RUFUS_SCHEDULER_CLIENT) }

      it 'creates a rufus scheduler job' do
        subject.save.must_equal true
        job = RUFUS_SCHEDULER_CLIENT.job(recording_schedule.job_id)
        recording_schedule.job_id.must_equal job[:id]
      end
    end

    describe 'with a new recurring RecordingSchedule' do
      let(:recording_schedule) { recording_schedules(:recurring).dup.decorate }
      subject { Services::ScheduleNotifier.new(recording_schedule, RUFUS_SCHEDULER_CLIENT) }

      it 'creates a rufus scheduler job' do
        subject.save.must_equal true
        job = RUFUS_SCHEDULER_CLIENT.job(recording_schedule.job_id)
        recording_schedule.job_id.must_equal job[:id]
      end
    end

    describe 'with a existing one-time RecordingSchedule' do
      let(:recording_schedule) { recording_schedules(:one_time).decorate }
      subject { Services::ScheduleNotifier.new(recording_schedule, RUFUS_SCHEDULER_CLIENT) }

      it 'creates a rufus scheduler job' do
        subject.save.must_equal true
        job = RUFUS_SCHEDULER_CLIENT.job(recording_schedule.job_id)
        recording_schedule.job_id.must_equal job[:id]
      end
    end

    describe 'with a new one-time RecordingSchedule' do
      let(:recording_schedule) { recording_schedules(:one_time).dup.decorate }
      subject { Services::ScheduleNotifier.new(recording_schedule, RUFUS_SCHEDULER_CLIENT) }

      it 'creates a rufus scheduler job' do
        subject.save.must_equal true
        job = RUFUS_SCHEDULER_CLIENT.job(recording_schedule.job_id)
        recording_schedule.job_id.must_equal job[:id]
      end
    end
  end
end
