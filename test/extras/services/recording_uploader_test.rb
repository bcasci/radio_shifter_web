require 'test_helper'

class Dropbox::Client
  #attr_accessor :offset

  def initialize(*args)
    #@total_size = args.last
    #@offset = 0
  end

  # def upload(chunk_size)
  #   @offset += chunk_size
  # end

  # def total_size
  #   @total_size
  # end

  # A real upload returns this:
  # https://www.dropbox.com/developers/core/docs#commit-chunked-upload
  # I don't feel a need to represent this in a test as a failed
  # upload raises a DropboxError.
  def finish(target_path)
    {}
  end

  def upload_request(action, body, args={})
    {}
  end
end

class Services::RecordingUploaderTest < ActiveSupport::TestCase

  def beanstalk_connection
    @beanstalk_connection ||= Backburner::Connection.new(Backburner.configuration.beanstalk_url)
  end

  let(:recording_schedule) { recording_schedules(:recurring) }
  let(:file_name) { "#{Rails.root}/test/fixtures/audio.mp3" }

  before do
    WebMock.stub_request(:get, "https://api.dropbox.com/users/get_current_account").
      with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Authorization'=>'Bearer sz7d0vzl0ltm5528', 'User-Agent'=>'OfficialDropboxRubySDK/1.6.4'}).
      to_return(
        :status => 200,
        :body => File.read("#{Rails.root}/test/fixtures/dropbox/account_info.json"),
        :headers => {})
  end

  describe '.upload' do
    subject do
      Services::RecordingUploader
    end

    describe 'with a RecordingSchedule instance' do
      it 'uploads a file to Dropbox' do
        response = subject.upload(recording_schedule, file_name)

        response.must_be_instance_of Hash
      end
    end

    describe 'with a RecordingSchedule id' do
      it 'uploads a file to Dropbox' do
        response = subject.upload(recording_schedule.id, file_name)

        response.must_be_instance_of Hash
      end
    end

    describe 'called via async' do
      let(:beanstalk_tube_name) { [Backburner.configuration.tube_namespace, Backburner.configuration.primary_queue].join('.') }

      after do
        beanstalk_connection.tubes[beanstalk_tube_name].clear
      end

      it 'queues a job in Beanstalk' do
        subject.async.upload(recording_schedule.id, file_name)
        tube_stats = beanstalk_connection.tubes[beanstalk_tube_name].stats
        tube_stats['current_jobs_ready'].must_equal 1
      end
    end

    describe 'when Dropbox account is not authorized' do
      before do
        WebMock.stub_request(:any, "https://api.dropbox.com/1/account/info").to_raise(Dropbox::ClientError)
      end

      it 'raises a Dropbox::ClientError' do
        proc { subject.upload(recording_schedule, file_name) }.must_raise Dropbox::ClientError
      end
    end
  end

  describe '#upload' do
    subject do
      Services::RecordingUploader.new(
        recording_schedule,
        File.new(file_name,'r')
      )
    end

    it 'uploads a file to Dropbox' do
      response = subject.upload

      response.must_be_instance_of Hash
    end

    describe 'when Dropbox account is not authorized' do
      before do
        WebMock.stub_request(:any, "https://api.dropbox.com/1/account/info").to_raise(Dropbox::ClientError)
      end

      it 'raises a DropboxAuthError' do
        proc { subject.upload }.must_raise Dropbox::ClientError
      end
    end
  end
end
