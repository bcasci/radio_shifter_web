require 'test_helper'

class Scheduler::ServerBootstrapperTest < ActiveSupport::TestCase
  describe 'Scheduler::ServerBootstrapper' do
    setup do
      one_time_schedule = recording_schedules(:one_time)
      one_time_schedule.start_on = Date.today + 1
      one_time_schedule.save
      recurring_schedule = recording_schedules(:recurring)
    end

    let(:rufus_scheduler) { Rufus::Scheduler.new }
    let(:server) { Scheduler::Server.new(rufus_scheduler) }
    subject { Scheduler::ServerBootstrapper.new(server, RecordingSchedule)}

    it 'should bootstrap the server' do
      subject.bootstrap
      job_ids_in_rufus = rufus_scheduler.jobs.map {|j| j.id}.sort
      job_ids_in_db = RecordingSchedule.all.map {|j| j.job_id}.sort

      rufus_scheduler.jobs.count.must_equal 2
      job_ids_in_db.must_equal job_ids_in_rufus
    end
  end
end
