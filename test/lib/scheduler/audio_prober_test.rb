require 'test_helper'

class Scheduler::AudioProberTest < ActiveSupport::TestCase
  describe 'Scheduler::AudioProber' do
    let (:input_file) { File.expand_path("../../../fixtures/audio.mp3", __FILE__) }
    subject { Scheduler::AudioProber.new('ffprobe', input_file) }

    describe '#bitrate' do
      it 'converts file' do
        subject.bitrate.must_equal 48000
      end
    end

    describe '#probe' do
      it 'converts file' do
        attibutes = subject.probe
        attibutes.must_be_instance_of Hash
        attibutes.empty?.must_equal false
      end
    end
  end
end
