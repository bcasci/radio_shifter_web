require 'test_helper'

class Scheduler::JobExecutorTest < ActiveSupport::TestCase
  STUB_TIME = Time.at(1421211586.440586)
  RUNNING_PID = 1234

  describe 'Scheduler::JobExecutor' do
    let(:job) { stub(last_time: STUB_TIME, next_time: STUB_TIME + 1.day) }
    let(:recording_schedule) { recording_schedules(:recurring).tap { |r| r.pid = nil } }

    subject { Scheduler::JobExecutor.new(recording_schedule, job) }

    describe '#execute' do
      describe 'when the recording schedule does not have a running process' do
        before do
          mock_recording_process = stub(pid: RUNNING_PID, spawn: nil, running?: false)
          Scheduler::RecordingProcess.stubs(:new).with(is_a(RecordingSchedule)).returns(mock_recording_process)
        end

        it 'updates the recording schedule with process information' do
          subject.execute

          recording_schedule.last_run_at.must_equal job.last_time
          recording_schedule.next_run_at.must_equal job.next_time
          recording_schedule.changed?.must_equal false
        end
      end

      describe 'when the recording schedule has a running process' do
        before do
          mock_recording_process = stub(pid: RUNNING_PID, spawn: nil, running?: true)
          Scheduler::RecordingProcess.stubs(:new).with(is_a(RecordingSchedule)).returns(mock_recording_process)
        end

        let(:last_updated_at) { recording_schedule.updated_at }

        it 'does not updates the recording schedule' do
          subject.execute

          recording_schedule.updated_at.must_be_same_as last_updated_at
        end
      end
    end
  end
end
