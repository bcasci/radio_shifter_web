require 'test_helper'

class Scheduler::WavAudioConverterTest < ActiveSupport::TestCase
  describe 'Scheduler::WavAudioConverter' do
    let (:unconverted_files) { ['/tmp/part_1.mp3', '/tmp/part_2.mp3']}
    let (:converted_files) { ['/tmp/part_1.wav', '/tmp/part_2.wav']}
    subject { Scheduler::WavAudioConverter.new('ffmpeg', unconverted_files) }

    before do
      file_name = 'audio.mp3'
      test_file = File.expand_path("../../../fixtures/#{file_name}", __FILE__)
      unconverted_files.each do |unconverted_file|
        %x[cp #{test_file} #{unconverted_file}]
      end
    end

    after do
      %x[rm #{ unconverted_files.join(' ') }]
      if subject.converted_files.present?
        %x[rm #{ subject.converted_files.join(' ') }]
      end
    end

    describe '#convert' do
      describe 'when the convert command runs successfully' do
        it 'converts files' do
          converted_files = subject.convert
          converted_files.must_equal converted_files
          converted_files.each do |converted_file|
            File.exists?(converted_file)
          end
        end
      end

      describe 'when the convert command runs successfully' do
        before do
          Scheduler::WavAudioConverter.any_instance.stubs(run_command: ['fake output', false])
        end
        it 'raises CommandLineError exception' do
          proc {subject.convert}.must_raise Scheduler::CommandLineError
        end
      end
    end
  end
end
