require 'test_helper'
require File.expand_path('../../../../lib/scheduler/server', __FILE__)
require File.expand_path('../../../../lib/scheduler/job_editor', __FILE__)

class Scheduler::ServerTest < ActiveSupport::TestCase
  describe 'Scheduler::Server' do
    let(:rufus_scheduler) { Rufus::Scheduler.new }
    subject { Scheduler::Server.new(rufus_scheduler) }

    before { rufus_scheduler.pause }

    describe '#cron' do
      let(:recurring_schedule) { recording_schedules(:recurring).decorate }

      it 'creates a job' do
        job = subject.cron(recurring_schedule)

        job.must_be_kind_of Hash
        rufus_scheduler.jobs.size.must_equal 1
      end
    end

    describe '#at' do
      let(:onetime_schedule) { recording_schedules(:recurring).decorate }

      it 'creates a job' do
        job = subject.cron(onetime_schedule)

        job.must_be_kind_of Hash
        rufus_scheduler.jobs.size.must_equal 1
      end
    end

    describe '#unschedule' do
      let(:onetime_schedule) { recording_schedules(:recurring).decorate }

      it 'unschedules a job' do
        job = subject.at(onetime_schedule)

        rufus_scheduler.jobs.size.must_equal 1
        subject.unschedule(job[:id])
        rufus_scheduler.jobs.size.must_equal 0
      end
    end

    describe '#job' do
      let(:onetime_schedule) { recording_schedules(:recurring).decorate }

      it 'returns a job with a matching id' do
        new_job = subject.at(onetime_schedule)
        found_job = subject.job(new_job[:id])

        new_job[:id].must_equal(found_job[:id])
      end
    end

    describe '#jobs' do
      let(:onetime_schedule) { recording_schedules(:recurring).decorate }
      let(:recurring_schedule) { recording_schedules(:recurring).decorate }

      it 'returns jobs matching ids' do
        new_jobs = []
        new_jobs << subject.at(onetime_schedule)
        new_jobs << subject.at(recurring_schedule)
        new_job_ids = new_jobs.map {|job| job[:id] }.sort
        found_jobs = subject.jobs(new_job_ids)
        found_job_ids = found_jobs.map {|job| job[:id] }.sort

        found_jobs.size.must_equal(2)
        found_job_ids.must_equal(new_job_ids)
      end
    end
  end
end
