require 'test_helper'

class Scheduler::RecordingProcessTest < ActiveSupport::TestCase
  ACTIVE_PID = Process.pid

  describe 'Scheduler::RecordingProcess' do
    before do
      if active_pid
        FileUtils.mkdir_p(Rails.root.join("tmp/pids/recordings"))
        File.open(recording_schedule.pid_file_name, 'w') {|f| f.write(active_pid) }
      end
      Process.stubs(:spawn).returns(ACTIVE_PID)
    end

    after do
      if File.exists?(recording_schedule.pid_file_name)
        File.delete(recording_schedule.pid_file_name)
      end
    end

    let(:recording_schedule) { recording_schedules(:recurring).decorate }

    subject { Scheduler::RecordingProcess.new(recording_schedule) }

    describe '#spawn' do
      describe 'when PID is active' do
        let(:active_pid) { ACTIVE_PID }

        it 'does not spawn a process' do
          subject.spawn.must_be_nil
        end
      end

      describe 'when PID is not active' do
        let(:active_pid) { nil }

        it 'spawns a process' do
          recording_schedule.pid = 100000

          subject.spawn.class.must_equal Process::Waiter
        end
      end

      describe 'when PID is stale' do
        let(:active_pid) { 123456789 }

        it 'spawns a process' do
          subject.spawn.class.must_equal Process::Waiter
        end
      end
    end


    describe '#running?' do
      describe 'when PID is active' do
        let(:active_pid) { ACTIVE_PID }

        it 'return true' do
          subject.running?.must_equal true
        end
      end

      describe 'when PID is stale' do
        let(:active_pid) { 123456789 }
        it 'return true' do

          subject.running?.must_equal false
        end
      end

      describe 'when PID is not active' do
        let(:active_pid) { nil }
        it 'return false' do
          subject.running?.must_equal false
        end
      end
    end
  end
end
