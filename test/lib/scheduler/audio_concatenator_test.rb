require 'test_helper'

class Scheduler::AudioConcatenatorTest < ActiveSupport::TestCase
  describe 'Scheduler::AudioConcatenator' do
    let (:input_files) { ['/tmp/part_1.wav', '/tmp/part_2.wav']}
    let (:output_file) { '/tmp/output.wav' }
    subject { Scheduler::AudioConcatenator.new('ffmpeg', input_files, output_file) }

    before do
      file_name = 'audio.mp3'
      source_file = File.expand_path("../../../fixtures/#{file_name}", __FILE__)

      input_files.each do |input_file|
        %x[ffmpeg -y -i #{source_file} #{input_file} 2>&1]
      end
    end

    after do
      files = input_files << output_file
      existing_files = files.select { |f| File.exists?(f) }
      File.delete(*existing_files)
    end

    describe '#concatenate' do
      describe 'when concatenatation command runs successfully' do
        it 'creates a wav file' do
          subject.concatenate.must_equal true
          File.exists?(output_file).must_equal true
        end
      end

      describe 'when the concatenatation command fails' do
        before do
          Scheduler::AudioConcatenator.any_instance.stubs(run_command: ['fake output', false])
        end

        it 'raises CommandLineError exception' do
          proc {subject.concatenate}.must_raise Scheduler::CommandLineError
        end
      end
    end
  end
end
