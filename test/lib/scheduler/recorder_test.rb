require 'test_helper'

class Scheduler::RecorderTest < ActiveSupport::TestCase
  describe 'Scheduler::Recorder' do

    subject { Scheduler::Recorder.new('http://localhost:4567/audio.mp3', 10, '/tmp') }

    describe '#command' do
      describe 'with non-playlist url' do
        it 'does not contain -playlist in the command' do
          subject.command.wont_include '-playlist'
        end
      end

      Scheduler::Recorder::PLAYLIST_FORMATS.each do |playlist_format|
        describe "with playlist url format #{playlist_format}" do
          subject { Scheduler::Recorder.new("http://localhost:4567/audio#{playlist_format}", 10, '/tmp') }

          it "contains -playlist in the command" do
            # It's supposed to be bad form to test a private method, but
            subject.command.must_include '-playlist'
          end
        end
      end
    end

    describe '#start' do
      describe 'when there are no interuptions' do
        it 'generates a dump file' do
          subject.start

          subject.dump_files.size.must_equal 1
          File.exists?(subject.dump_files.first).must_equal true
        end
      end

      describe 'when there are interuptions' do
        subject { Scheduler::Recorder.new('http://localhost:4567/audio.mp3', 20, '/tmp') }
        it 'generates a dump file' do
          Process.spawn("sleep 10 && pkill mplayer")
          subject.start
          # Heads up. The test audio files need to be long enough or
          # mplayer won't bother creating dump files.
          subject.dump_files.size.must_equal 2
        end
      end
    end
  end
end
