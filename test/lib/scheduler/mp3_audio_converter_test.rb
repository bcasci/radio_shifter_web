require 'test_helper'

class Scheduler::Mp3AudioConverterTest < ActiveSupport::TestCase
  describe 'Scheduler::Mp3AudioConverter' do
    let (:unconverted_file) { '/tmp/unconverted.wav' }
    let (:output_file) { '/tmp/converted.mp3' }
    subject { Scheduler::Mp3AudioConverter.new('ffmpeg', unconverted_file, output_file, '48k') }

    before do
      file_name = 'audio.mp3'
      test_file = File.expand_path("../../../fixtures/#{file_name}", __FILE__)
      `ffmpeg -y -i #{test_file} #{unconverted_file} 2>&1`
    end

    after do
      File.delete(unconverted_file) if File.exists?(unconverted_file)
      File.delete(output_file) if File.exists?(output_file)
    end

    describe '#convert' do
      describe 'when the convert command runs successfully' do
        it 'converts file' do
          subject.convert.must_equal true
          File.exists?(output_file)
        end
      end

      describe 'when the convert command fails' do
        before do
          Scheduler::Mp3AudioConverter.any_instance.stubs(run_command: ['fake output', false])
        end
        it 'raises CommandLineError exception' do
          proc {subject.convert}.must_raise Scheduler::CommandLineError
        end
      end
    end
  end
end
