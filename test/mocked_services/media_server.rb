require 'rubygems'
require 'sinatra'

get '/audio.mp3' do
  mp3_path = File.expand_path("../../fixtures/audio.mp3", __FILE__)
  stream do |out|
    File.open mp3_path, "rb" do |io|
      pos = io.tell
      while buffer = io.read(6000)
        out << buffer
        pos = io.tell
        sleep 1
      end
    end
  end

  #File.read(mp3_path)
end
