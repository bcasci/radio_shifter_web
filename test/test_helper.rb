ENV['RAILS_ENV'] ||= 'test'
ENV['BEANSTALK_PORT'] ||= '11301'

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/spec"
require "minitest/mock"
require 'mocha/mini_test'
require "webmock/minitest"

WebMock.disable_net_connect!(:allow_localhost => true)

class ActiveRecord::Base
  mattr_accessor :shared_connection
  @@shared_connection = nil

  def self.connection
    @@shared_connection || retrieve_connection
  end
end
ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection

class ProcessSpawner

  def initialize
    @pids = []
  end

  def spawn(command)
    @pids << Process.spawn("#{command}")
  end

  def kill_all
    @pids.each do |pid|
      Process.kill('INT', pid)
    end
  end
end


process_spawner = ProcessSpawner.new
process_spawner.spawn("bundle exec ruby #{Rails.root}/test/mocked_services/media_server.rb")
process_spawner.spawn("bundle exec #{Rails.root}/bin/scheduler")
process_spawner.spawn("beanstalkd -p 11301")

sleep 10

MiniTest.after_run { process_spawner.kill_all }

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  extend MiniTest::Spec::DSL

  # Remove ActiveSupport::TestCase describe method, which will get replaced
  # by the MiniTest::Spec describe.
  class << self
    remove_method :describe
  end

  # Tell MiniTest::Spec to use ActiveSupport::TestCase when describing an ActiveRecord model.
  register_spec_type self do |desc|
    desc < ActiveRecord::Base if desc.is_a? Class
  end
end
