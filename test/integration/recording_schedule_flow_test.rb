require 'integration_test_helper'

class RecordingScheduleFlowTest < ActionDispatch::IntegrationTest
  describe 'Recording schedules list', js: true do
    describe 'accessed by user not logged in' do
      it 'sees the login page' do
        visit recording_schedules_path

        page.current_path.must_equal new_user_session_path
      end
    end

    describe 'accessed by user with recording schedules' do
      before do
        sign_in(users(:joe))
        visit recording_schedules_path
      end

      it 'sees recording schedules' do
        page.must_have_css '.recording_schedule_card', count: 2
        page.must_have_link 'Edit', count: 2
        page.must_have_link 'Delete', count: 2
      end

      it 'can click edit' do
        edit_link = first(:link, 'Edit')
        edit_href = edit_link['href']
        edit_link.click

        page.current_path.must_equal edit_href
      end
    end
  end

  describe 'Recording schedules edit' do
    before do
      @joe = users(:joe)
      sign_in(@joe)
    end

    describe 'with recurring true' do
      before do
        @recording_schedule = @joe.recording_schedules.where(recurring: true).first
        visit edit_recording_schedule_path(@recording_schedule)
      end

      it 'initializes the weekday check box list' do
        unchecked = Date::DAYNAMES_MAP.values - @recording_schedule.days_of_week

        Date::DAYNAMES_MAP.each_pair do |day, ordinal_weekday|
          if @recording_schedule.days_of_week.include?(ordinal_weekday)
            page.must_have_checked_field day
          else
            page.must_have_unchecked_field day
          end
        end
      end

      it 'accepts schedule changes' do
        page.fill_in 'recording_schedule[name]', with: 'Changed Name'
        page.fill_in 'recording_schedule[stream_url]', with: 'http://changed.url.com'
        page.fill_in 'recording_schedule[start_time]', with: '2:00am'
        page.fill_in 'recording_schedule[stop_time]', with: '4:00am'

        Date::DAYNAMES_MAP.each_pair do |day, ordinal_weekday|
          page.find("#recording_schedule_days_of_week_#{ordinal_weekday}").set(false)
        end

        page.find("#recording_schedule_days_of_week_0").set(true)
        page.click_button 'Save'

        page.current_path.must_equal recording_schedules_path
        page.must_have_content 'Changed Name'
        page.must_have_content 'Between 2:00am and 4:00am on Sunday'
      end
    end

    describe 'with recurring false' do
      before do
        @recording_schedule = @joe.recording_schedules.where(recurring: false).first
        visit edit_recording_schedule_path(@recording_schedule)
      end

      it 'hides recurring tab' do
        page.must_have_css 'ul.tabs li.current', text: 'Record Once'

        page.must_have_css '#tab_repeat_recording', visible: false
      end
    end
  end
end
