require 'integration_test_helper'

class RegistrationFlowTest < ActionDispatch::IntegrationTest
  describe 'User registration' do
    before { visit new_user_session_path }
    after { User.where(uid: '12345').delete_all }
    it 'sees a dropbox button' do
      page.must_have_link 'Sign in using Dropbox'
    end

    it 'sees a time zone select after registration' do
      page.click_link 'Sign in using Dropbox'
      page.find('.selectize-control').click

      page.must_have_content(' (GMT-05:00) Eastern Time (US & Canada)')
    end

    describe 'without selecting a time zone' do
      it 'cannot go to dashboard' do
        page.click_link 'Sign in using Dropbox'
        page.click_link 'Dashboard'
        page.current_path.must_equal time_zone_path
      end
    end

    describe 'after selecting a time zone' do
      it 'sees the recording schedules page' do
        page.click_link 'Sign in using Dropbox'
        page.find('.selectize-control').click
        page.find('.selectize-dropdown-content div', text: '(GMT-05:00) Eastern Time (US & Canada)').click
        page.click_button 'Save'

        page.current_path.must_equal recording_schedules_path
      end
    end
  end
end
