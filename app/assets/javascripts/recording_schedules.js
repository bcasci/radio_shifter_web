var calculateRecordingDuration = function() {
  var startingAt = $('#recording_schedule_start_time').timepicker('getSecondsFromMidnight');
  var endingAt = $('#recording_schedule_stop_time').timepicker('getSecondsFromMidnight');
  var seconds = endingAt - startingAt;
  // If seconds are negative then compensate by cross a day boundary
  if (seconds < 0) {
      seconds += 86400;
  }
  return seconds;
};

var disableStopTimeSelect = function() {
  $('#recording_schedule_stop_time').prop('disabled', true);
}

var enableStopTimeSelect = function() {
  $('#recording_schedule_stop_time').prop('disabled', false);
}

var setupStopTimePicker = function () {
  var $stopTime = $('#recording_schedule_stop_time');
  var currentVal = $stopTime.val();
  $stopTime
    .unbind()
    .removeData()
    .val('')
    .timepicker({
      'minTime': $('#recording_schedule_start_time').val(),
      'step': 15})
    .on('change', function() {
      $('#recording_schedule_duration').val(calculateRecordingDuration());
    })
    .val(currentVal);
}

var setupTimePickers = function() {
  $('#recording_schedule_start_time').timepicker({ 'step': 15 });
  $('.recording_schedules.new').each(function() {
    disableStopTimeSelect();
  });
  $('.recording_schedules.edit, .recording_schedules.create, .recording_schedules.update').each(function() {
    setupStopTimePicker();
  });
  $('#recording_schedule_start_time').on('change', function() {
    enableStopTimeSelect();
    setupStopTimePicker();
  });
}

var setupRecordingTabClicks = function() {
  //for page load
  $('#recording_schedule_recurring').val(
    $('.tabs li.current a').data('recurring')
  );

  //for tab clicks
  $('.tabs a').click(function() {
    console.log($(this).data('recurring'));
    $('#recording_schedule_recurring').val(
      $(this).data('recurring')
    );
  });
}

$(document).ready (function() {
  $('body.recording_schedules').each(function() {
    setupTimePickers();
    setupRecordingTabClicks();
  });
});
