module AsyncHandlers
  class ErrorHandler
    def self.process(exception, class_name, job_arguments, job)
      if exception.is_a?(DropboxAuthError)
        # Bury the job
        job.bury
        # invalidate user's schedule
      end
    end
  end
end