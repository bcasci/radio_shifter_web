class FormBuilders::ExtendedFormBuilder < ActionView::Helpers::FormBuilder
  def label(method, text = nil, options = {}, &block)
    append_error_style_class_option(method, options)
    super
  end

  def text_field(method, options = {})
    append_error_style_class_option(method, options)
    super
  end

  def select(method, choices = nil, options = {}, html_options = {}, &block)
    append_error_style_class_option(method, options)
    super
  end

  private

  def append_error_style_class_option(method, options)
    if @object.errors[method.to_sym].present?
      options[:class] = [options[:class], 'error'].compact.join(' ')
    end
  end
end
