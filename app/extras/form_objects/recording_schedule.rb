module FormObjects
  class RecordingSchedule
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :days_of_week, :duration, :end_time, :id, :name, :start_on, :start_time, :stream_url, :user
    attr_reader :object

    validates :name, :start_time, :stream_url, :end_time, presence: true

    def initialize(params_or_recording_schedule = {})
      @params = if params_or_recording_schedule.is_a?(::RecordingSchedule)
        @object = params_or_recording_schedule
        params_or_recording_schedule.attributes
      elsif params_or_recording_schedule.is_a?(Hash)
        @object = ::RecordingSchedule.new
        params_or_recording_schedule || {}
      end

      @params.each_pair do |k, v|
        self.instance_variable_set("@#{k}", v) unless v.nil?
      end

    end

    def persisted?
      @object.persisted?
    end

    def save
      self.valid?
      if self.valid?
        @object.attributes = recording_schedule_params
        @object.save
      end
    end

    private

    def recording_schedule_params
      {
        user_id: user.id,
        name: @params[:name],
        stream_url: @params[:stream_url],
        starts_at: starts_at,
        duration: duration,
        days_of_week: @params[:days_of_week],
        inactive_at: nil
      }
    end

    def starts_at
       Time.parse("#{self.start_on} #{self.start_time}")
    end
  end
end
