module Services
  class RecordingUploader
    include Backburner::Performable

    def initialize(recording_schedule, file)
      @recording_schedule = recording_schedule
      @file = file
    end

    def self.upload(recording_schedule, file_path)
      schedule = recording_schedule.is_a?(RecordingSchedule) ? recording_schedule : RecordingSchedule.find(recording_schedule)
      puts "Starting uploading for #{schedule.id} - #{file_path}"
      file = File.new(file_path, 'r')
      recording_uploader = new(schedule, file)
      recording_uploader.upload
    end

    def upload
      client = Dropbox::Client.new(@recording_schedule.user.oauth_token)
      authorize!(client)

      retries = 0
      chunk_size = 1024 * 1024 * 4
      dropbox_target_path = generate_permanent_filename(@file)

      upload_session_cursor = client.start_upload_session(@file.read(chunk_size))
      until @file.eof?
        client.append_upload_session(upload_session_cursor, @file.read(chunk_size))
      end
      client.finish_upload_session(upload_session_cursor, dropbox_target_path, [])
    end

    # def upload
    #   # Upload the file
    #   client = DropboxClient.new(@recording_schedule.user.oauth_token)
    #   # early exit if there is no authorization

    #   authorize!(client)

    #   uploader = client.get_chunked_uploader(@file, @file.size)
    #   retries = 0
    #   chunk_size = 1024 * 1024 * 4
    #   dropbox_target_path = generate_permanent_filename(@file)

    #   while uploader.offset < uploader.total_size
    #     begin
    #       uploader.upload(chunk_size)
    #     rescue SocketError, DropboxError => e
    #       puts e
    #       if retries > 20
    #         # TODO Error uploading, giving up.
    #         break
    #       end
    #       # Error uploading, trying again...
    #       retries += 1
    #       sleep 10
    #     end
    #   end

    #   uploader.finish(dropbox_target_path)
    # end

    private

    def authorize!(client)
      authorized = nil
      begin
        authorized = client.get_current_account.present?
      rescue Dropbox::ClientError => e
        raise e
      end
      authorized
    end

    def generate_permanent_filename(file)
      estimated_start_time = Time.now.in_time_zone(@recording_schedule.user.time_zone) - @recording_schedule.duration
      prefix = estimated_start_time.strftime('%Y-%m-%d')
      temp_name = File.basename(file)
      temp_name.gsub(
        File.basename(temp_name, '.*'),
        "/recordings/#{@recording_schedule.name.parameterize}/#{prefix}-#{@recording_schedule.name.parameterize}"
      )
    end
  end
end
