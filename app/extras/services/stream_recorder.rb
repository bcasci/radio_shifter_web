module Services
  class StreamRecorder
    def initialize(recording_schedule)
      @recording_schedule = recording_schedule
    end

    def start
      command = "cd #{Rails.root}/bin && FFMPEG_CMD=/root/bin/ffmpeg FFPROBE_CMD=/root/bin/ffprobe nohup sh ./record_stream.sh #{@recording_schedule.stream_url} #{@recording_schedule.duration} #{@recording_schedule.id} > /dev/null &"
      system(command)
    end
  end
end
