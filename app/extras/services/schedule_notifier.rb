module Services
  class ScheduleNotifier
    def initialize(recording_schedule, rufus_scheduler_client)
      @recording_schedule = recording_schedule
      @rufus_scheduler_client = rufus_scheduler_client
    end

    def destroy
     @recording_schedule.destroy && unschedule_job
    end

    def save(attributes = nil)
      if attributes
        @recording_schedule.attributes = attributes
      end
      @recording_schedule.save && create_or_update_job
    end

    private

    def create_or_update_job
      job = if @recording_schedule.recurring?
        @rufus_scheduler_client.cron(@recording_schedule)
      else
        @rufus_scheduler_client.at(@recording_schedule)
      end
      @recording_schedule.update_attribute(:job_id, job[:id])
    end

    def unschedule_job
      if @recording_schedule.job_id.present?
        begin
          @rufus_scheduler_client.unschedule(@recording_schedule.job_id)
        rescue ArgumentError => e
          # Not sure if there should be any concern over the a job missing from Rufus.
        end
      end
    end
  end
end
