module Services
  class CreateUser
    def self.from_omniauth(auth)
      criteria = { provider: auth.provider, email: auth.info.email }
      user = User.where(criteria).first_or_initialize do |user|
        user.password = Devise.friendly_token[0,20]
      end

      user.tap do |u|
        u.oauth_token = auth.credentials.token
        u.save
      end
    end
  end
end
