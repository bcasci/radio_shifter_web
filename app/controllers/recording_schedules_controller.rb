class RecordingSchedulesController < ApplicationController
  before_action :authenticate_user!
  before_action :redirect_to_time_zone, if: Proc.new { current_user.time_zone.blank? }

  def index
    @recording_schedules = RecordingSchedule.where(user_id: current_user.id).order(:name).decorate
  end

  def new
    @recording_schedule = RecordingSchedule.new
  end

  def create
    @recording_schedule = RecordingSchedule.new(recording_schedule_params).decorate
    @recording_schedule.user = current_user
    notifier = Services::ScheduleNotifier.new(@recording_schedule, RUFUS_SCHEDULER_CLIENT)

    if notifier.save
      redirect_to recording_schedules_path, flash: { success: 'Recording schedule added.' }
    else
      flash.now[:error] = "There were some problems with this recording schedule."
      render :new
    end
  end

  def destroy
    @recording_schedule = RecordingSchedule.find(params[:id]).decorate
    notifier = Services::ScheduleNotifier.new(@recording_schedule, RUFUS_SCHEDULER_CLIENT)
    flash_options = {}

    if notifier.destroy
      flash_options[:success] = 'Recording schedule removed.'
    else
      flash_options[:success] = 'Recording schedule not removed.'
    end

    redirect_to recording_schedules_path, flash_options
  end

  def edit
    @recording_schedule = RecordingSchedule.where(id: params[:id], user_id: current_user.id).first
  end

  def update
    @recording_schedule = RecordingSchedule.where(id: params[:id], user_id: current_user.id)
      .first
      .decorate
    notifier = Services::ScheduleNotifier.new(@recording_schedule, RUFUS_SCHEDULER_CLIENT)

    if notifier.save(recording_schedule_params)
      redirect_to recording_schedules_path, flash: { success: 'Changes saved.' }
    else
      flash.now[:error] = "There were some problems with this recording schedule."
      render :edit
    end
  end

  private

  def recording_schedule_params
    strip_blank_from_days_of_week(
      params
        .require(:recording_schedule)
        .permit(:duration, :stop_time, :name, :recurring,
         :start_on, :start_time, :stream_url, days_of_week: []
        )
    )
  end

  def redirect_to_time_zone
    redirect_to time_zone_path
  end

  def strip_blank_from_days_of_week(permittied_params)
    if permittied_params[:days_of_week].present?
      permittied_params[:days_of_week]
        .reject! {|i|i.blank?}
        .collect!(&:to_i)
    end
    permittied_params
  end
end
