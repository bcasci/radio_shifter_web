class TimeZonesController < ApplicationController
  before_action :authenticate_user!

  def show

  end

  def update
    if current_user.update_attributes(time_zone_params)
      redirect_to session[:return_to] || recording_schedules_path
    else
      flash[:error] = "There were some problems"
      render :update
    end
  end

  private

  def time_zone_params
    params.require(:user).permit(:time_zone)
  end
end
