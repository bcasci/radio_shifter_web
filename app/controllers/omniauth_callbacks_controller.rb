class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def dropbox_oauth2
    @user = Services::CreateUser.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
       sign_in_and_redirect @user, :event => :authentication
       @user.remember_me!
       set_flash_message(:notice, :success, :kind => "Dropbox") if is_navigational_format?
     else
       redirect_to new_user_session_url
     end
  end
end
