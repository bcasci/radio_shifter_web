class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Forcing SSL in dev since it's mandatory in production.
  # For details see config/initializers/ssl_patch_for_development.rb
  force_ssl if Rails.env.development?

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || recording_schedules_path
  end

end
