class RecordingSchedules::LogsController < ApplicationController
  before_action :authenticate_user!

  def show
    recording_schedule = current_user.recording_schedules.find(params[:recording_schedule_id]).decorate
    @log_content = if File.exists?(recording_schedule.logfile_name)
      File.readlines(recording_schedule.logfile_name)[0..500].join
    end || 'The logfile is empty.'
  end

end
