class RecordingScheduleDecorator < Draper::Decorator
  delegate_all

  def human_readable_schedule
    sentence_parts = ["Between #{self.start_time} and #{self.stop_time} on"]

    if object.recurring?
      sentence_parts << self.object.days_of_week.map { |d|
       Date::DAYNAMES_MAP.key(d)
      }.to_sentence
    else
      sentence_parts << object.start_on.to_s(:long)
    end
    sentence_parts.join(' ')
  end

  def logfile_name
    Rails.root.join("log/recordings/#{self.id}/recording.log")
  end

  def pid_file_name
    Rails.root.join("tmp/pids/recordings/#{self.id}.pid")
  end

  def rufus_cron_line
    parsed_time = Time.strptime(self.object.start_time,"%I:%M%P")
    "#{parsed_time.min} #{parsed_time.hour} * * #{self.object.days_of_week.join(',')} #{user.iana_time_zone}"
  end

  def rufus_at_line
    "#{self.object.start_on} #{self.object.start_time} #{user.iana_time_zone}"
  end

  def expired?
    if self.object.recurring?
      false
    else
      now = Time.now.in_time_zone(self.object.user.time_zone)
      ("#{self.object.start_on} #{self.object.start_time} ").in_time_zone(self.object.user.time_zone) < now
    end
  end

end
