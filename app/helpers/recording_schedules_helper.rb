module RecordingSchedulesHelper
  def recording_status_class(recording_schedule)
    if Scheduler::RecordingProcess.new(recording_schedule).running?
      'recording'
    else
      'sleeping'
    end
  end

  def start_on_options
    (Date.today..Date.today+14).to_a.map {|d| [d.to_s(:long),d.to_s]}
  end
end
