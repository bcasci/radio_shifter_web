module ApplicationHelper
  def body_class
    "#{controller_name} #{action_name}"
  end

  def current_class(return_class)
    if return_class
      'current'
    end
  end

  def rot13(content)
    content.tr("A-Ma-mN-Zn-z","N-Zn-zA-Ma-m").html_safe
  end

end
