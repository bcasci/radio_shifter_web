class RecordingSchedule < ActiveRecord::Base
  serialize :days_of_week, JSON

  belongs_to :user

  validates :name, :start_time, :stop_time, presence: true
  validates :stream_url, url: true, presence: true
  validate :days_of_week_must_exist, if: :recurring?
  validates :start_on, presence: true, unless: :recurring?

  private

  def days_of_week_must_exist
    # Get the intersection of arrays.
    intersection = Date::DAYNAMES_MAP.values & (self.days_of_week || [])

    if self.days_of_week != intersection || self.days_of_week.blank?
      errors.add(:days_of_week, 'must be one or more days of the week')
    end
  end
end
