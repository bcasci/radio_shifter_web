class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable

  has_many :recording_schedules

  def iana_time_zone
    ActiveSupport::TimeZone::MAPPING[self.time_zone]
  end
end
